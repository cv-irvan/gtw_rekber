package rekening

type service struct {
	repo RekeningRepository
}

type RekeningService interface {
	GetRekening(Email string) ([]Rekening, error)
	CreateRekening(BodyRekening BodyRekening) (Rekening, error)
	PutRekening(Id int, BodyRekening BodyRekening) (Rekening, error)
	DeleteRekening(Id int) (Rekening, error)
}

func ConnectService(repo RekeningRepository) *service {
	return &service{repo}
}

func (service *service) GetRekening(Email string) ([]Rekening, error) {
	return service.repo.GetRekening(Email)
}

func (service *service) CreateRekening(BodyRekening BodyRekening) (Rekening, error) {
	Rekenings := Rekening{
		AtasNama:   BodyRekening.AtasNama,
		NoRekening: BodyRekening.NoRekening,
		IdBank:     BodyRekening.IdBank,
		NamaBank:   BodyRekening.NamaBank,
		Email:      BodyRekening.Email,
	}

	return service.repo.CreateRekening(Rekenings)
}

func (service *service) PutRekening(Id int, BodyRekening BodyRekening) (Rekening, error) {
	Rekenings := Rekening{
		AtasNama:   BodyRekening.AtasNama,
		NoRekening: BodyRekening.NoRekening,
		IdBank:     BodyRekening.IdBank,
		NamaBank:   BodyRekening.NamaBank,
		Email:      BodyRekening.Email,
	}

	return service.repo.PutRekening(Id, Rekenings)
}

func (service *service) DeleteRekening(Id int) (Rekening, error) {
	return service.repo.DeleteRekening(Id)
}
