package rekening

import (
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type RekeningRepository interface {
	GetRekening(Email string) ([]Rekening, error)
	CreateRekening(Rekening Rekening) (Rekening, error)
	PutRekening(Id int, rekening Rekening) (Rekening, error)
	DeleteRekening(Id int) (Rekening, error)
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) GetRekening(Email string) ([]Rekening, error) {
	var rekening []Rekening

	result := repo.db.Where("email = ?", Email).Find(&rekening).Error

	return rekening, result
}

func (repo *repository) CreateRekening(rekening Rekening) (Rekening, error) {
	result := repo.db.Create(&rekening).Error

	return rekening, result
}

func (repo *repository) PutRekening(Id int, Data Rekening) (Rekening, error) {
	var rekening Rekening

	result := repo.db.Find(&rekening, Id).Error

	rekening.NoRekening = Data.NoRekening
	rekening.AtasNama = Data.AtasNama
	rekening.IdBank = Data.IdBank
	rekening.NamaBank = Data.NamaBank
	rekening.Email = Data.Email

	result = repo.db.Save(&rekening).Error

	return rekening, result
}

func (repo *repository) DeleteRekening(Id int) (Rekening, error) {
	var rekening Rekening

	result := repo.db.Find(&rekening, Id).Error

	result = repo.db.Delete(&rekening).Error

	return rekening, result
}
