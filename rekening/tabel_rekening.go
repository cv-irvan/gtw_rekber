package rekening

type Rekening struct {
	Id         int
	NoRekening string
	AtasNama   string
	IdBank     int
	NamaBank   string
	Email      string
}
