package rekening

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type rekeningHandler struct {
	rekeningService RekeningService
}

func ConnectHandler(rekeningService RekeningService) *rekeningHandler {
	return &rekeningHandler{rekeningService}
}

func (handler *rekeningHandler) PostRekening(ctx *gin.Context) {
	var body BodyRekening

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": body,
			},
		)
		return
	}

	rekening, err := handler.rekeningService.CreateRekening(body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if rekening.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":       "00",
			"rc_desc":  "Berhasil Menyimpan Rekening",
			"rekening": rekening,
		},
	)
}

func (handler *rekeningHandler) GetRekening(ctx *gin.Context) {
	email := ctx.Query("email")

	rekening, err := handler.rekeningService.GetRekening(email)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":       "00",
			"rc_desc":  "Berhasil Mengambil Rekening",
			"rekening": rekening,
		},
	)
}

func (handler *rekeningHandler) PutRekening(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	var body BodyRekening

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": "Gagal",
			},
		)
		return
	}

	rekening, err := handler.rekeningService.PutRekening(iid, body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if rekening.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":       "00",
			"rc_desc":  "Berhasil Merubah Rekening",
			"rekening": rekening,
		},
	)
}

func (handler *rekeningHandler) DeleteRekening(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	rekening, err := handler.rekeningService.DeleteRekening(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if rekening.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":       "00",
			"rc_desc":  "Berhasil Menghapus Rekening",
			"rekening": rekening,
		},
	)
}
