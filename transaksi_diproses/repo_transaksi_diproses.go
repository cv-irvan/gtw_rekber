package transaksidiproses

import (
	transaksiselesai "pustaka-api/transaksi_selesai"

	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type TransaksiDiprosesRepository interface {
	GetTransaksiDiproses(Email string) ([]TransaksiDiproses, error)
	GetPindahSelesai(Id int) (TransaksiDiproses, error)
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) GetTransaksiDiproses(Email string) ([]TransaksiDiproses, error) {
	var transaksiProses []TransaksiDiproses
	var transaksiProsesDua []TransaksiDiproses

	repo.db.Where("email_pembeli = ?", Email).Find(&transaksiProsesDua)

	result := repo.db.Where("email_penjual = ?", Email).Find(&transaksiProses).Error

	return append(transaksiProses, transaksiProsesDua...), result

}

func (repo *repository) GetPindahSelesai(Id int) (TransaksiDiproses, error) {
	var transaksiDiproses TransaksiDiproses
	var transaksiSelesai transaksiselesai.TransaksiSelesai

	result := repo.db.Find(&transaksiDiproses, Id).Error

	transaksiSelesai = transaksiselesai.TransaksiSelesai(transaksiDiproses)

	transaksiSelesai.StatustTiga = true
	transaksiSelesai.Note = "Uang sedang diteruskan ke penjual"

	result = repo.db.Save(&transaksiSelesai).Error

	result = repo.db.Delete(&transaksiDiproses).Error

	return TransaksiDiproses(transaksiSelesai), result
}
