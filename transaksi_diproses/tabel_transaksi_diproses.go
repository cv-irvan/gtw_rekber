package transaksidiproses

import "time"

type TransaksiDiproses struct {
	Id                  int
	IdTransaksi         string
	NamaProduk          string
	HargaProduk         string
	NamaBank            string
	NomerRekening       string
	NamaPemilikRekening string
	BiayaAdmin          string
	BiayaAdminOleh      string
	NamaPenjual         string
	NomerPenjual        string
	EmailPenjual        string
	NamaPembeli         string
	NomerPembeli        string
	EmailPembeli        string
	StatusSatu          bool
	StatusDua           bool
	StatustTiga         bool
	StatusEmpat         bool
	StatusBatal         bool
	Note                string
	ImgBarang           string
	ImgBuktiBayar       string
	TanggalTransaksi    time.Time
}
