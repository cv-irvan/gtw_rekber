package transaksidiproses

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type transaksiDiprosesHandler struct {
	transaksiDirosesService TransaksiDiprosesService
}

func ConnectHandler(transaksiProsesService TransaksiDiprosesService) *transaksiDiprosesHandler {
	return &transaksiDiprosesHandler{transaksiProsesService}
}

func (handler *transaksiDiprosesHandler) GetTransaksiDiproses(ctx *gin.Context) {
	email := ctx.Query("email")

	transaksiProses, err := handler.transaksiDirosesService.GetTransaksiDiproses(email)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":              "00",
			"rc_desc":         "Berhasil Mengambil TransaksiDiproses",
			"transaksiProses": transaksiProses,
		},
	)
}

func (handler *transaksiDiprosesHandler) GetPindahSelesai(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	transaksiDiproses, err := handler.transaksiDirosesService.GetPindahSelesai(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if transaksiDiproses.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":                "00",
			"rc_desc":           "Berhasil Merubah TransaksiDiproses",
			"transaksiDiproses": transaksiDiproses,
		},
	)
}
