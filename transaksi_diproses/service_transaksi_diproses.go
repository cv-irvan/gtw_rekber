package transaksidiproses

type service struct {
	repo TransaksiDiprosesRepository
}

type TransaksiDiprosesService interface {
	GetTransaksiDiproses(Email string) ([]TransaksiDiproses, error)
	GetPindahSelesai(Id int) (TransaksiDiproses, error)
}

func ConnectService(repo TransaksiDiprosesRepository) *service {
	return &service{repo}
}

func (service *service) GetTransaksiDiproses(Email string) ([]TransaksiDiproses, error) {
	return service.repo.GetTransaksiDiproses(Email)
}

func (service *service) GetPindahSelesai(Id int) (TransaksiDiproses, error) {

	return service.repo.GetPindahSelesai(Id)
}
