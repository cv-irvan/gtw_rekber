package bank

type service struct {
	repo BankRepository
}

type BankService interface {
	GetBank() ([]Bank, error)
	CreateBank(BodyBank BodyBank) (Bank, error)
	PutBank(Id int, BodyBank BodyBank) (Bank, error)
	DeleteBank(Id int) (Bank, error)
}

func ConnectService(repo BankRepository) *service {
	return &service{repo}
}

func (service *service) GetBank() ([]Bank, error) {
	return service.repo.GetBank()
}

func (service *service) CreateBank(BodyBank BodyBank) (Bank, error) {
	Banks := Bank{
		NamaBank: BodyBank.NamaBank,
	}

	return service.repo.CreateBank(Banks)
}

func (service *service) PutBank(Id int, BodyBank BodyBank) (Bank, error) {
	Banks := Bank{
		NamaBank: BodyBank.NamaBank,
	}

	return service.repo.PutBank(Id, Banks)
}

func (service *service) DeleteBank(Id int) (Bank, error) {
	return service.repo.DeleteBank(Id)
}
