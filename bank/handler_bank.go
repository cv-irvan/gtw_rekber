package bank

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type bankHandler struct {
	bankService BankService
}

func ConnectHandler(bankService BankService) *bankHandler {
	return &bankHandler{bankService}
}

func (handler *bankHandler) PostBank(ctx *gin.Context) {
	var body BodyBank

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": "Formuli tidak boleh kosong",
			},
		)
		return
	}

	bank, err := handler.bankService.CreateBank(body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if bank.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Menyimpan Bank",
			"bank":    bank,
		},
	)
}

func (handler *bankHandler) GetBank(ctx *gin.Context) {

	bank, err := handler.bankService.GetBank()

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Mengambil Bank",
			"bank":    bank,
		},
	)
}

func (handler *bankHandler) PutBank(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	var body BodyBank

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": "Gagal",
			},
		)
		return
	}

	bank, err := handler.bankService.PutBank(iid, body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if bank.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Merubah Bank",
			"bank":    bank,
		},
	)
}

func (handler *bankHandler) DeleteBank(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	bank, err := handler.bankService.DeleteBank(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if bank.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Menghapus Bank",
			"bank":    bank,
		},
	)
}
