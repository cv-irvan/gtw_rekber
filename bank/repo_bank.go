package bank

import (
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type BankRepository interface {
	GetBank() ([]Bank, error)
	CreateBank(Bank Bank) (Bank, error)
	PutBank(Id int, bank Bank) (Bank, error)
	DeleteBank(Id int) (Bank, error)
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) GetBank() ([]Bank, error) {
	var bank []Bank

	result := repo.db.Find(&bank).Error

	return bank, result
}

func (repo *repository) CreateBank(bank Bank) (Bank, error) {
	result := repo.db.Create(&bank).Error

	return bank, result
}

func (repo *repository) PutBank(Id int, Data Bank) (Bank, error) {
	var bank Bank

	result := repo.db.Find(&bank, Id).Error

	bank.NamaBank = Data.NamaBank

	result = repo.db.Save(&bank).Error

	return bank, result
}

func (repo *repository) DeleteBank(Id int) (Bank, error) {
	var bank Bank

	result := repo.db.Find(&bank, Id).Error

	result = repo.db.Delete(&bank).Error

	return bank, result
}
