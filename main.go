package main

import (
	"log"
	"pustaka-api/bank"
	"pustaka-api/profil"
	"pustaka-api/rekening"
	rekeningrekber "pustaka-api/rekening_rekber"
	transaksibaru "pustaka-api/transaksi_baru"
	transaksidiproses "pustaka-api/transaksi_diproses"
	transaksiselesai "pustaka-api/transaksi_selesai"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
)

func main() {

	dsn := "root:@tcp(127.0.0.1:3306)/rekber?charset=utf8mb4&parseTime=True&loc=Local"
	db, err := gorm.Open(mysql.Open(dsn), &gorm.Config{})

	if err != nil {
		log.Fatal("gagal koneksi db")
	}

	db.AutoMigrate(&profil.Profil{})
	db.AutoMigrate(&bank.Bank{})
	db.AutoMigrate(&rekening.Rekening{})
	db.AutoMigrate(&transaksibaru.TransaksiBaru{})
	db.AutoMigrate(&transaksibaru.CekPembayaran{})
	db.AutoMigrate(&transaksidiproses.TransaksiDiproses{})
	db.AutoMigrate(&transaksiselesai.TransaksiSelesai{})
	db.AutoMigrate(&rekeningrekber.RekeningRekber{})

	profilRepository := profil.ConnectRepository(db)
	profilService := profil.ConnectService(profilRepository)
	profilHandler := profil.ConnectHandler(profilService)

	bankRepository := bank.ConnectRepository(db)
	bankService := bank.ConnectService(bankRepository)
	bankHandler := bank.ConnectHandler(bankService)

	rekeningRepository := rekening.ConnectRepository(db)
	rekeningService := rekening.ConnectService(rekeningRepository)
	rekeningHandler := rekening.ConnectHandler(rekeningService)

	rekeningRekberRepository := rekeningrekber.ConnectRepository(db)
	rekeningRekberService := rekeningrekber.ConnectService(rekeningRekberRepository)
	rekeningRekberHandler := rekeningrekber.ConnectHandler(rekeningRekberService)

	transaksiBaruRepository := transaksibaru.ConnectRepository(db)
	transaksiBaruService := transaksibaru.ConnectService(transaksiBaruRepository)
	transaksiBaruHandler := transaksibaru.ConnectHandler(transaksiBaruService)

	transaksiDiprosesRepository := transaksidiproses.ConnectRepository(db)
	transaksiDiprosesService := transaksidiproses.ConnectService(transaksiDiprosesRepository)
	transaksiDiprosesHandler := transaksidiproses.ConnectHandler(transaksiDiprosesService)

	transaksiSelesaiRepository := transaksiselesai.ConnectRepository(db)
	transaksiSelesaiService := transaksiselesai.ConnectService(transaksiSelesaiRepository)
	transaksiSelesaiHandler := transaksiselesai.ConnectHandler(transaksiSelesaiService)

	root := gin.Default()

	root.POST("/profil", profilHandler.PostProfil)
	root.GET("/profil", profilHandler.GetProfil)
	root.PUT("/profil", profilHandler.PutProfil)
	root.DELETE("/profil", profilHandler.DeleteProfil)

	root.POST("/bank", bankHandler.PostBank)
	root.GET("/bank", bankHandler.GetBank)
	root.PUT("/bank", bankHandler.PutBank)
	root.DELETE("/bank", bankHandler.DeleteBank)

	root.POST("/rekening", rekeningHandler.PostRekening)
	root.GET("/rekening", rekeningHandler.GetRekening)
	root.PUT("/rekening", rekeningHandler.PutRekening)
	root.DELETE("/rekening", rekeningHandler.DeleteRekening)

	root.POST("/rekening_rekber", rekeningRekberHandler.PostRekeningRekber)
	root.GET("/rekening_rekber", rekeningRekberHandler.GetRekeningRekber)
	root.PUT("/rekening_rekber", rekeningRekberHandler.PutRekeningRekber)
	root.DELETE("/rekening_rekber", rekeningRekberHandler.DeleteRekeningRekber)

	root.POST("/transaksi_baru", transaksiBaruHandler.PostTransaksiBaru)
	root.GET("/transaksi_baru", transaksiBaruHandler.GetTransaksiBaru)
	root.GET("/transaksi_all_cek_bayar", transaksiBaruHandler.GetAllCekBayar)
	root.GET("/transaksi_cek_bayar", transaksiBaruHandler.GetCekBayar)
	root.GET("/transaksi_pindah_proses", transaksiBaruHandler.GetPindahProses)
	root.GET("/batalkan_transaksi", transaksiBaruHandler.GetBatal)

	root.GET("/transaksi_diproses", transaksiDiprosesHandler.GetTransaksiDiproses)
	root.GET("/transaksi_pindah_selesai", transaksiDiprosesHandler.GetPindahSelesai)

	root.GET("/transaksi_selesai", transaksiSelesaiHandler.GetTransaksiSelesai)
	root.GET("/bayar_penjual", transaksiSelesaiHandler.GetBayarPenjual)

	root.Run(":8888")
}
