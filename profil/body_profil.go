package profil

type BodyPostProfil struct {
	Nama      string `json:"nama" binding:"required"`
	Email     string `json:"email" binding:"required"`
	NoHp      string `json:"nohp" binding:"required"`
	ImgProfil string `json:"img_profil"`
}

type BodyPutProfil struct {
	Id        int    `json:"id" binding:"required"`
	Nama      string `json:"nama" binding:"required"`
	Email     string `json:"email" binding:"required"`
	NoHp      string `json:"nohp" binding:"required"`
	ImgProfil string `json:"img_profil"`
}
