package profil

type service struct {
	repo ProfilRepository
}

type ProfilService interface {
	GetProfil(Email string) (Profil, error)
	CreateProfil(BodyProfil BodyPostProfil) (Profil, error)
	PutProfil(Id int, BodyProfil BodyPostProfil) (Profil, error)
	DeleteProfil(Id int) (Profil, error)
}

func ConnectService(repo ProfilRepository) *service {
	return &service{repo}
}

func (service *service) GetProfil(Email string) (Profil, error) {
	return service.repo.GetProfil(Email)
}

func (service *service) CreateProfil(BodyProfil BodyPostProfil) (Profil, error) {
	Profils := Profil{
		Nama:      BodyProfil.Nama,
		Email:     BodyProfil.Email,
		NoHp:      BodyProfil.NoHp,
		ImgProfil: BodyProfil.ImgProfil,
	}

	return service.repo.CreateProfil(Profils)
}

func (service *service) PutProfil(Id int, BodyProfil BodyPostProfil) (Profil, error) {
	Profils := Profil{
		Nama:      BodyProfil.Nama,
		Email:     BodyProfil.Email,
		NoHp:      BodyProfil.NoHp,
		ImgProfil: BodyProfil.ImgProfil,
	}

	return service.repo.PutProfil(Id, Profils)
}

func (service *service) DeleteProfil(Id int) (Profil, error) {
	return service.repo.DeleteProfil(Id)
}
