package profil

type Profil struct {
	Id        int
	Nama      string
	Email     string
	NoHp      string
	ImgProfil string
}
