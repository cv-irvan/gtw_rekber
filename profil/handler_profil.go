package profil

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type profilHandler struct {
	profilService ProfilService
}

func ConnectHandler(profilService ProfilService) *profilHandler {
	return &profilHandler{profilService}
}

func (handler *profilHandler) PostProfil(ctx *gin.Context) {
	var body BodyPostProfil

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": body,
			},
		)
		return
	}

	profil, err := handler.profilService.CreateProfil(body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if profil.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Menyimpan Profil",
			"profil":  profil,
		},
	)
}

func (handler *profilHandler) GetProfil(ctx *gin.Context) {
	email := ctx.Query("email")

	profil, err := handler.profilService.GetProfil(email)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if profil.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Mengambil Profil",
			"profil":  profil,
		},
	)
}

func (handler *profilHandler) PutProfil(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	var body BodyPostProfil

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": "Gagal",
			},
		)
		return
	}

	profil, err := handler.profilService.PutProfil(iid, body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if profil.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Merubah Profil",
			"profil":  profil,
		},
	)
}

func (handler *profilHandler) DeleteProfil(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	profil, err := handler.profilService.DeleteProfil(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if profil.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":      "00",
			"rc_desc": "Berhasil Menghapus Profil",
			"profil":  profil,
		},
	)
}
