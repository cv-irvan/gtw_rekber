package profil

import (
	"gorm.io/gorm"
)

type ProfilRepository interface {
	GetProfil(Email string) (Profil, error)
	CreateProfil(Profil Profil) (Profil, error)
	PutProfil(Id int, Profil Profil) (Profil, error)
	DeleteProfil(Id int) (Profil, error)
}

type repository struct {
	db *gorm.DB
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) GetProfil(Email string) (Profil, error) {
	var profil Profil

	result := repo.db.Where("email = ?", Email).Find(&profil).Error

	return profil, result
}

func (repo *repository) CreateProfil(profil Profil) (Profil, error) {
	result := repo.db.Create(&profil).Error

	return profil, result
}

func (repo *repository) PutProfil(Id int, Data Profil) (Profil, error) {
	var profil Profil

	result := repo.db.Find(&profil, Id).Error

	profil.Nama = Data.Nama
	profil.Email = Data.Email
	profil.NoHp = Data.NoHp 
	profil.ImgProfil = Data.ImgProfil

	result = repo.db.Save(&profil).Error

	return profil, result
}

func (repo *repository) DeleteProfil(Id int) (Profil, error) {
	var profil Profil

	result := repo.db.Find(&profil, Id).Error

	result = repo.db.Delete(&profil).Error

	return profil, result
}
