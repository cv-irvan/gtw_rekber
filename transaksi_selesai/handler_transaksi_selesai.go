package transaksiselesai

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type transaksiSelesaiHandler struct {
	transaksiDirosesService TransaksiSelesaiService
}

func ConnectHandler(transaksiProsesService TransaksiSelesaiService) *transaksiSelesaiHandler {
	return &transaksiSelesaiHandler{transaksiProsesService}
}

func (handler *transaksiSelesaiHandler) GetTransaksiSelesai(ctx *gin.Context) {
	email := ctx.Query("email")

	transaksiProses, err := handler.transaksiDirosesService.GetTransaksiSelesai(email)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":              "00",
			"rc_desc":         "Berhasil Mengambil TransaksiSelesai",
			"transaksiProses": transaksiProses,
		},
	)
}

func (handler *transaksiSelesaiHandler) GetBayarPenjual(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	transaksiSelesai, err := handler.transaksiDirosesService.GetBayarPenjual(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if transaksiSelesai.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":               "00",
			"rc_desc":          "Berhasil Merubah TransaksiSelesai",
			"transaksiSelesai": transaksiSelesai,
		},
	)
}
