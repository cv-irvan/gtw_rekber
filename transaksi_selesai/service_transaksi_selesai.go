package transaksiselesai

type service struct {
	repo TransaksiSelesaiRepository
}

type TransaksiSelesaiService interface {
	GetTransaksiSelesai(Email string) ([]TransaksiSelesai, error)
	GetBayarPenjual(Id int) (TransaksiSelesai, error)
}

func ConnectService(repo TransaksiSelesaiRepository) *service {
	return &service{repo}
}

func (service *service) GetTransaksiSelesai(Email string) ([]TransaksiSelesai, error) {
	return service.repo.GetTransaksiSelesai(Email)
}

func (service *service) GetBayarPenjual(Id int) (TransaksiSelesai, error) {

	return service.repo.GetBayarPenjual(Id)
}
