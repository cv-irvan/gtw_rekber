package transaksiselesai

import (
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type TransaksiSelesaiRepository interface {
	GetTransaksiSelesai(Email string) ([]TransaksiSelesai, error)
	GetBayarPenjual(Id int) (TransaksiSelesai, error)
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) GetTransaksiSelesai(Email string) ([]TransaksiSelesai, error) {
	var transaksiProses []TransaksiSelesai
	var transaksiProsesDua []TransaksiSelesai

	repo.db.Where("email_pembeli = ?", Email).Find(&transaksiProsesDua)

	result := repo.db.Where("email_penjual = ?", Email).Find(&transaksiProses).Error

	return append(transaksiProses, transaksiProsesDua...), result

}

func (repo *repository) GetBayarPenjual(Id int) (TransaksiSelesai, error) {

	var transaksiSelesai TransaksiSelesai

	result := repo.db.Find(&transaksiSelesai, Id).Error

	transaksiSelesai.StatusEmpat = true
	transaksiSelesai.Note = "Uang berhasil dikirim ke Penjual"

	result = repo.db.Save(&transaksiSelesai).Error

	return transaksiSelesai, result
}
