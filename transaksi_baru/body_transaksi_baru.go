package transaksibaru

type BodyTransaksiBaru struct {
	NamaProduk          string `json:"nama_produk"`
	HargaProduk         string `json:"harga_produk"`
	NamaBank            string `json:"nama_bank"`
	NomerRekening       string `json:"nomer_rekening"`
	NamaPemilikRekening string `json:"nama_pemilik_rekening"`
	BiayaAdmin          string `json:"biaya_admin"`
	BiayaAdminOleh      string `json:"biaya_admin_oleh"`
	NamaPenjual         string `json:"nama_penjual"`
	NomerPenjual        string `json:"nomer_penjual"`
	EmailPenjual        string `json:"email_penjual"`
	NamaPembeli         string `json:"nama_pembeli"`
	NomerPembeli        string `json:"nomer_pembeli"`
	EmailPembeli        string `json:"email_pembeli"`
	StatusSatu          bool   `json:"status_satu"`
	StatusDua           bool   `json:"status_dua"`
	StatustTiga         bool   `json:"statust_tiga"`
	StatusEmpat         bool   `json:"status_empat"`
	StatusBatal         bool   `json:"status_batal"`
	Note                string `json:"note"`
	ImgBarang           string `json:"img_barang"`
	ImgBuktiBayar       string `json:"img_bukti_bayar"`
}
