package transaksibaru

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type transaksiBaruHandler struct {
	transaksiBaruService TransaksiBaruService
}

func ConnectHandler(transaksiBaruService TransaksiBaruService) *transaksiBaruHandler {
	return &transaksiBaruHandler{transaksiBaruService}
}

func (handler *transaksiBaruHandler) PostTransaksiBaru(ctx *gin.Context) {
	var body BodyTransaksiBaru

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": body,
			},
		)
		return
	}

	transaksiBaru, err := handler.transaksiBaruService.CreateTransaksiBaru(body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if transaksiBaru.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":            "00",
			"rc_desc":       "Berhasil Menyimpan TransaksiBaru",
			"transaksiBaru": transaksiBaru,
		},
	)
}

func (handler *transaksiBaruHandler) GetTransaksiBaru(ctx *gin.Context) {
	email := ctx.Query("email")

	cekPembayaran, transaksiBaru, err := handler.transaksiBaruService.GetTransaksiBaru(email)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":            "00",
			"rc_desc":       "Berhasil Mengambil TransaksiBaru",
			"transaksiBaru": transaksiBaru,
			"cekPembayaran": cekPembayaran,
		},
	)
}

func (handler *transaksiBaruHandler) GetCekBayar(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)
	img := ctx.Query("img")

	transaksiBaru, err := handler.transaksiBaruService.GetCekBayar(iid, img)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if transaksiBaru.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":            "00",
			"rc_desc":       "Berhasil Merubah TransaksiBaru",
			"transaksiBaru": transaksiBaru,
		},
	)
}

func (handler *transaksiBaruHandler) GetAllCekBayar(ctx *gin.Context) {

	cekBayar, err := handler.transaksiBaruService.GetAllCekBayar()

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":        "00",
			"rc_desc":   "Berhasil Mengambil Cek Pembayaran",
			"transaksi": cekBayar,
		},
	)
}

func (handler *transaksiBaruHandler) GetPindahProses(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	transaksiBaru, err := handler.transaksiBaruService.GetPindahProses(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if transaksiBaru.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":            "00",
			"rc_desc":       "Berhasil Merubah TransaksiBaru",
			"transaksiBaru": transaksiBaru,
		},
	)
}

func (handler *transaksiBaruHandler) GetBatal(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	transaksiBaru, err := handler.transaksiBaruService.GetBatal(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if transaksiBaru.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":            "00",
			"rc_desc":       "Berhasil Merubah TransaksiBaru",
			"transaksiBaru": transaksiBaru,
		},
	)
}
