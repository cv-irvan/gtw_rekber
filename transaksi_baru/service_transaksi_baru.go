package transaksibaru

import (
	"fmt"
	"time"

	uuid "github.com/nu7hatch/gouuid"
)

type service struct {
	repo TransaksiBaruRepository
}

type TransaksiBaruService interface {
	GetTransaksiBaru(Email string) ([]CekPembayaran, []TransaksiBaru, error)
	CreateTransaksiBaru(BodyTransaksiBaru BodyTransaksiBaru) (TransaksiBaru, error)
	GetCekBayar(Id int, ImgBayar string) (TransaksiBaru, error)
	GetAllCekBayar() ([]CekPembayaran, error)
	GetPindahProses(Id int) (TransaksiBaru, error)
	GetBatal(Id int) (TransaksiBaru, error)
}

func ConnectService(repo TransaksiBaruRepository) *service {
	return &service{repo}
}

func (service *service) CreateTransaksiBaru(BodyTransaksiBaru BodyTransaksiBaru) (TransaksiBaru, error) {

	idTrx, err := uuid.NewV4()

	if err != nil {
		fmt.Println("Error:", err)
	}

	TransaksiBarus := TransaksiBaru{
		IdTransaksi:         idTrx.String(),
		NamaProduk:          BodyTransaksiBaru.NamaProduk,
		HargaProduk:         BodyTransaksiBaru.HargaProduk,
		NomerRekening:       BodyTransaksiBaru.NomerRekening,
		NamaPemilikRekening: BodyTransaksiBaru.NamaPemilikRekening,
		BiayaAdmin:          BodyTransaksiBaru.BiayaAdmin,
		BiayaAdminOleh:      BodyTransaksiBaru.BiayaAdminOleh,
		NamaPenjual:         BodyTransaksiBaru.NamaPenjual,
		NomerPenjual:        BodyTransaksiBaru.NomerPenjual,
		EmailPenjual:        BodyTransaksiBaru.EmailPenjual,
		NamaPembeli:         BodyTransaksiBaru.NamaPembeli,
		NomerPembeli:        BodyTransaksiBaru.NomerPembeli,
		EmailPembeli:        BodyTransaksiBaru.EmailPembeli,
		StatusSatu:          BodyTransaksiBaru.StatusSatu,
		StatusDua:           BodyTransaksiBaru.StatusDua,
		StatustTiga:         BodyTransaksiBaru.StatustTiga,
		StatusEmpat:         BodyTransaksiBaru.StatusEmpat,
		StatusBatal:         BodyTransaksiBaru.StatusBatal,
		Note:                BodyTransaksiBaru.Note,
		NamaBank:            BodyTransaksiBaru.NamaBank,
		ImgBarang:           BodyTransaksiBaru.ImgBarang,
		ImgBuktiBayar:       BodyTransaksiBaru.ImgBuktiBayar,
		TanggalTransaksi:    time.Now(),
	}

	return service.repo.CreateTransaksiBaru(TransaksiBarus)
}

func (service *service) GetTransaksiBaru(Email string) ([]CekPembayaran, []TransaksiBaru, error) {
	return service.repo.GetTransaksiBaru(Email)
}

func (service *service) GetCekBayar(Id int, ImgBayar string) (TransaksiBaru, error) {

	return service.repo.GetCekBayar(Id, ImgBayar)
}

func (service *service) GetAllCekBayar() ([]CekPembayaran, error) {

	return service.repo.GetAllCekBayar()
}

func (service *service) GetPindahProses(Id int) (TransaksiBaru, error) {

	return service.repo.GetPindahProses(Id)
}

func (service *service) GetBatal(Id int) (TransaksiBaru, error) {

	return service.repo.GetBatal(Id)
}
