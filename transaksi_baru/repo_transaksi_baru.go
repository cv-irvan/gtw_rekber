package transaksibaru

import (
	transaksidiproses "pustaka-api/transaksi_diproses"
	transaksiselesai "pustaka-api/transaksi_selesai"

	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type TransaksiBaruRepository interface {
	GetTransaksiBaru(Email string) ([]CekPembayaran, []TransaksiBaru, error)
	CreateTransaksiBaru(TransaksiBaru TransaksiBaru) (TransaksiBaru, error)
	GetCekBayar(Id int, ImgBayar string) (TransaksiBaru, error)
	GetAllCekBayar() ([]CekPembayaran, error)
	GetPindahProses(Id int) (TransaksiBaru, error)
	GetBatal(Id int) (TransaksiBaru, error)
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) CreateTransaksiBaru(transaksiBaru TransaksiBaru) (TransaksiBaru, error) {
	result := repo.db.Create(&transaksiBaru).Error

	return transaksiBaru, result
}

func (repo *repository) GetTransaksiBaru(Email string) ([]CekPembayaran, []TransaksiBaru, error) {
	var transaksiBaru []TransaksiBaru
	var transaksiBaruDua []TransaksiBaru
	var cekBayar []CekPembayaran
	var cekBayarDua []CekPembayaran

	repo.db.Where("email_pembeli = ?", Email).Find(&transaksiBaruDua)
	repo.db.Where("email_pembeli = ?", Email).Find(&cekBayar)
	repo.db.Where("email_penjual = ?", Email).Find(&cekBayarDua)

	result := repo.db.Where("email_penjual = ?", Email).Find(&transaksiBaru).Error

	return append(cekBayar, cekBayarDua...), append(transaksiBaru, transaksiBaruDua...), result
}

func (repo *repository) GetCekBayar(Id int, ImgBayar string) (TransaksiBaru, error) {
	var transaksiBaru TransaksiBaru
	var cekBayar CekPembayaran

	result := repo.db.Find(&transaksiBaru, Id).Error

	cekBayar = CekPembayaran(transaksiBaru)

	cekBayar.StatusEmpat = true
	cekBayar.Note = "Dalam proses pengecekan pembayaran"
	cekBayar.ImgBuktiBayar = ImgBayar

	result = repo.db.Save(&cekBayar).Error

	result = repo.db.Delete(&transaksiBaru).Error

	return TransaksiBaru(cekBayar), result
}

func (repo *repository) GetAllCekBayar() ([]CekPembayaran, error) {

	var cekBayar []CekPembayaran

	result := repo.db.Find(&cekBayar).Error

	return cekBayar, result
}

func (repo *repository) GetPindahProses(Id int) (TransaksiBaru, error) {
	var cekbayar CekPembayaran
	var transaksiDiproses transaksidiproses.TransaksiDiproses

	result := repo.db.Find(&cekbayar, Id).Error

	transaksiDiproses = transaksidiproses.TransaksiDiproses(cekbayar)

	transaksiDiproses.StatusDua = true
	transaksiDiproses.StatusEmpat = false
	transaksiDiproses.Note = "hubungi penjual untuk meminta nomer resi jika menggunakan jasa pengiriman, jika barang telah sampai mohon untuk memeriksa barang sebelum menekan tombol selesaikan transaksi"

	result = repo.db.Save(&transaksiDiproses).Error

	result = repo.db.Delete(&cekbayar).Error

	return TransaksiBaru(transaksiDiproses), result
}

func (repo *repository) GetBatal(Id int) (TransaksiBaru, error) {
	var transaksiBaru TransaksiBaru
	var transaksiSelesai transaksiselesai.TransaksiSelesai

	result := repo.db.Find(&transaksiBaru, Id).Error

	transaksiSelesai = transaksiselesai.TransaksiSelesai(transaksiBaru)

	transaksiSelesai.StatusSatu = false
	transaksiSelesai.StatusBatal = true
	transaksiSelesai.Note = "Transaksi Dibatalkan dikarenakan 3 hari telah berlalu dan belum ada konfirmasi dari pembeli"

	result = repo.db.Save(&transaksiSelesai).Error

	result = repo.db.Delete(&transaksiBaru).Error

	return TransaksiBaru(transaksiSelesai), result
}
