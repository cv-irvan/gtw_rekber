package rekeningrekber

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
)

type rekeningRekberHandler struct {
	rekeningRekberService RekeningRekberService
}

func ConnectHandler(rekeningRekberService RekeningRekberService) *rekeningRekberHandler {
	return &rekeningRekberHandler{rekeningRekberService}
}

func (handler *rekeningRekberHandler) PostRekeningRekber(ctx *gin.Context) {
	var body BodyRekeningRekber

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": "Formuli tidak boleh kosong",
			},
		)
		return
	}

	rekeningRekber, err := handler.rekeningRekberService.CreateRekeningRekber(body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if rekeningRekber.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":             "00",
			"rc_desc":        "Berhasil Menyimpan RekeningRekber",
			"rekeningRekber": rekeningRekber,
		},
	)
}

func (handler *rekeningRekberHandler) GetRekeningRekber(ctx *gin.Context) {

	rekeningRekber, err := handler.rekeningRekberService.GetRekeningRekber()

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":             "00",
			"rc_desc":        "Berhasil Mengambil RekeningRekber",
			"rekeningRekber": rekeningRekber,
		},
	)
}

func (handler *rekeningRekberHandler) PutRekeningRekber(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	var body BodyRekeningRekber

	chack := ctx.ShouldBindJSON(&body)

	if chack != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": "Gagal",
			},
		)
		return
	}

	rekeningRekber, err := handler.rekeningRekberService.PutRekeningRekber(iid, body)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if rekeningRekber.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":             "00",
			"rc_desc":        "Berhasil Merubah RekeningRekber",
			"rekeningRekber": rekeningRekber,
		},
	)
}

func (handler *rekeningRekberHandler) DeleteRekeningRekber(ctx *gin.Context) {
	id := ctx.Query("id")
	iid, err := strconv.Atoi(id)

	rekeningRekber, err := handler.rekeningRekberService.DeleteRekeningRekber(iid)

	if err != nil {
		ctx.JSON(
			http.StatusBadRequest, gin.H{
				"rc":      "06",
				"rc_desc": err,
			},
		)
		return
	}

	if rekeningRekber.Id == 0 {
		ctx.JSON(
			http.StatusOK, gin.H{
				"rc":      "06",
				"rc_desc": "Data tidak ditemukan",
			},
		)
		return
	}

	ctx.JSON(
		http.StatusOK, gin.H{
			"rc":             "00",
			"rc_desc":        "Berhasil Menghapus RekeningRekber",
			"rekeningRekber": rekeningRekber,
		},
	)
}
