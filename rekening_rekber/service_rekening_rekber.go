package rekeningrekber

type service struct {
	repo RekeningRekberRepository
}

type RekeningRekberService interface {
	GetRekeningRekber() ([]RekeningRekber, error)
	CreateRekeningRekber(BodyRekeningRekber BodyRekeningRekber) (RekeningRekber, error)
	PutRekeningRekber(Id int, BodyRekeningRekber BodyRekeningRekber) (RekeningRekber, error)
	DeleteRekeningRekber(Id int) (RekeningRekber, error)
}

func ConnectService(repo RekeningRekberRepository) *service {
	return &service{repo}
}

func (service *service) GetRekeningRekber() ([]RekeningRekber, error) {
	return service.repo.GetRekeningRekber()
}

func (service *service) CreateRekeningRekber(BodyRekeningRekber BodyRekeningRekber) (RekeningRekber, error) {
	RekeningRekbers := RekeningRekber{
		AtasNama:   BodyRekeningRekber.AtasNama,
		NoRekening: BodyRekeningRekber.NoRekening,
		IdBank:     BodyRekeningRekber.IdBank,
		NamaBank:   BodyRekeningRekber.NamaBank,
	}

	return service.repo.CreateRekeningRekber(RekeningRekbers)
}

func (service *service) PutRekeningRekber(Id int, BodyRekeningRekber BodyRekeningRekber) (RekeningRekber, error) {
	RekeningRekbers := RekeningRekber{
		AtasNama:   BodyRekeningRekber.AtasNama,
		NoRekening: BodyRekeningRekber.NoRekening,
		IdBank:     BodyRekeningRekber.IdBank,
		NamaBank:   BodyRekeningRekber.NamaBank,
	}

	return service.repo.PutRekeningRekber(Id, RekeningRekbers)
}

func (service *service) DeleteRekeningRekber(Id int) (RekeningRekber, error) {
	return service.repo.DeleteRekeningRekber(Id)
}
