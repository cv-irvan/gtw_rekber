package rekeningrekber

type BodyRekeningRekber struct {
	NoRekening string `json:"no_rekening" binding:"required"`
	AtasNama   string `json:"atas_nama" binding:"required"`
	IdBank     int    `json:"id_bank" binding:"required"`
	NamaBank   string `json:"nama_bank" binding:"required"`
}
