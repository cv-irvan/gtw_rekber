package rekeningrekber

import (
	"gorm.io/gorm"
)

type repository struct {
	db *gorm.DB
}

type RekeningRekberRepository interface {
	GetRekeningRekber() ([]RekeningRekber, error)
	CreateRekeningRekber(RekeningRekber RekeningRekber) (RekeningRekber, error)
	PutRekeningRekber(Id int, rekeningRekber RekeningRekber) (RekeningRekber, error)
	DeleteRekeningRekber(Id int) (RekeningRekber, error)
}

func ConnectRepository(db *gorm.DB) *repository {
	return &repository{db}
}

func (repo *repository) GetRekeningRekber() ([]RekeningRekber, error) {
	var rekeningRekber []RekeningRekber

	result := repo.db.Find(&rekeningRekber).Error

	return rekeningRekber, result
}

func (repo *repository) CreateRekeningRekber(rekeningRekber RekeningRekber) (RekeningRekber, error) {
	result := repo.db.Create(&rekeningRekber).Error

	return rekeningRekber, result
}

func (repo *repository) PutRekeningRekber(Id int, Data RekeningRekber) (RekeningRekber, error) {
	var rekeningRekber RekeningRekber

	result := repo.db.Find(&rekeningRekber, Id).Error

	rekeningRekber.NoRekening = Data.NoRekening
	rekeningRekber.AtasNama = Data.AtasNama
	rekeningRekber.IdBank = Data.IdBank
	rekeningRekber.NamaBank = Data.NamaBank

	result = repo.db.Save(&rekeningRekber).Error

	return rekeningRekber, result
}

func (repo *repository) DeleteRekeningRekber(Id int) (RekeningRekber, error) {
	var rekeningRekber RekeningRekber

	result := repo.db.Find(&rekeningRekber, Id).Error

	result = repo.db.Delete(&rekeningRekber).Error

	return rekeningRekber, result
}
