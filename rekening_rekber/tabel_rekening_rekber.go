package rekeningrekber

type RekeningRekber struct {
	Id         int
	NoRekening string
	AtasNama   string
	IdBank     int
	NamaBank   string
}
